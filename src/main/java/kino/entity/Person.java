package kino.entity;

import org.hibernate.annotations.WhereJoinTable;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "persons", schema = "movies")
public class Person {

    private int id;
    private String firstName;
    private String lastName;
    private List<Movie> directed;
    private List<Movie> starred;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "first_name")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Basic
    @Column(name = "last_name")
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(
            name = "cast_and_crew",
            joinColumns = @JoinColumn(name = "id_person"),
            inverseJoinColumns = @JoinColumn(name = "id_film")
    )
    @WhereJoinTable(clause = "id_role = 2")
    public List<Movie> getDirected() {
        System.out.println("directed size = " + directed.size());
        return directed;
    }

    public void setDirected(List<Movie> directed) {
        this.directed = directed;
    }

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(
            name = "cast_and_crew",
            joinColumns = @JoinColumn(name = "id_person"),
            inverseJoinColumns = @JoinColumn(name = "id_film")
    )
    @WhereJoinTable(clause = "id_role = 1")
    public List<Movie> getStarred() {
        System.out.println("starred size = " + directed.size());
        return starred;
    }

    public void setStarred(List<Movie> starred) {
        this.starred = starred;
    }
}
