package kino.entity;

import kino.dto.ShortMovie;

import java.util.List;
import java.util.stream.Collectors;

public class DetailedGenre {

    private int id;
    private String name;
    private List<ShortMovie> movies;

    public DetailedGenre(Genre genre) {
        id = genre.getId();
        name = genre.getName();
        movies = genre.getMovies().stream().map(ShortMovie::new).collect(Collectors.toList());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ShortMovie> getMovies() {
        return movies;
    }

    public void setMovies(List<ShortMovie> movies) {
        this.movies = movies;
    }
}
