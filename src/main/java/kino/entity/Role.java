package kino.entity;

public enum Role {

    ACTOR(1),
    DIRECTOR(2),
    ;


    private int id;

    Role(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }


    @Override
    public String toString() {
        return String.valueOf(id);
    }
}
