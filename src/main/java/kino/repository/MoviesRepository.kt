package kino.repository

import kino.dao.MoviesDao
import kino.dto.DetailedMovie
import kino.dto.ShortMovie

class MoviesRepository(private val moviesDao: MoviesDao) {

    fun getAllMovies() = moviesDao.getAll().map { ShortMovie(it) }

    fun getById(id: Int) = DetailedMovie(moviesDao.getById(id))
}