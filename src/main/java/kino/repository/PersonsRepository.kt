package kino.repository

import kino.dao.PersonsDao
import kino.dto.DetailedPerson
import kino.dto.ShortPerson

class PersonsRepository(private val personsDao: PersonsDao) {

    fun getAll() = personsDao.getAll().map { ShortPerson(it) }

    fun getById(id: Int) = DetailedPerson(personsDao.getById(id))
}