package kino.repository

import kino.dao.CountriesDao
import kino.dto.DetailedCountry
import kino.dto.ShortCountry

class CountriesRepository(private val countriesDao: CountriesDao) {

    fun getAll() = countriesDao.getAll().map { ShortCountry(it) }

    fun getById(id: Int) = DetailedCountry(countriesDao.getById(id))

}