package kino.repository

import kino.dao.GenresDao
import kino.dto.ShortGenre
import kino.entity.DetailedGenre
import kino.exceptions.RequaredParamsNotPresentException

class GenresRepository(private val genresDao: GenresDao) {

    fun getAll() = genresDao.getAll().map { ShortGenre(it) }

    fun getById(id: Int?, name: String?): DetailedGenre {
        return when {
            id != null -> DetailedGenre(genresDao.getById(id))
            name != null -> DetailedGenre(genresDao.getByName(name))
            else -> throw RequaredParamsNotPresentException()
        }
    }

}