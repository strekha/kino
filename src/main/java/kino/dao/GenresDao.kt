package kino.dao

import kino.entity.Genre
import org.springframework.stereotype.Repository
import javax.persistence.EntityManager

interface GenresDao : Dao<Genre> {

    fun getByName(name: String): Genre

}

@Repository
open class GenresDaoImpl(private val manager: EntityManager) : BaseDao<Genre>(manager), GenresDao {

    override fun getByName(name: String): Genre {
        val query = manager.createQuery("FROM Genre g WHERE g.name = :name", Genre::class.java)
        query.setParameter("name", name)
        return query.resultList.first()
    }

    override fun getById(id: Int) = manager.find(Genre::class.java, id)

    override fun getAll() = manager.createQuery("FROM Genre", Genre::class.java).resultList

}