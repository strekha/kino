package kino.dao

import kino.entity.Movie
import org.springframework.stereotype.Repository
import javax.persistence.EntityManager

abstract class BaseDao<T>(private val manager: EntityManager) : Dao<T> {

    override fun add(entity: T) = manager.persist(entity)

    override fun update(entity: T): T = manager.merge(entity)

    override fun delete(id: Int) = manager.remove(getById(id))
}

@Repository
open class MoviesDaoImpl(private val manager: EntityManager) : BaseDao<Movie>(manager), MoviesDao {

    override fun filter(id: Int, genre: String): List<Movie> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    override fun getAll() = manager.createQuery("FROM Movie", Movie::class.java).resultList

    override fun getById(id: Int) = manager.find(Movie::class.java, id)

}