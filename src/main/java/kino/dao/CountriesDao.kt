package kino.dao

import kino.entity.Country
import org.springframework.stereotype.Repository
import javax.persistence.EntityManager

interface CountriesDao : Dao<Country>

@Repository
open class CountriesDaoImpl(private val manager: EntityManager) : BaseDao<Country>(manager), CountriesDao {

    override fun getById(id: Int) = manager.find(Country::class.java, id)

    override fun getAll() = manager.createQuery("FROM Country", Country::class.java).resultList

}