package kino.dao

import kino.entity.Movie

interface Dao<T> {

    fun add(entity: T)

    fun getById(id: Int): T

    fun update(entity: T): T

    fun delete(id: Int)

    fun getAll(): List<T>
}

interface MoviesDao : Dao<Movie> {
    fun filter(id: Int, genre: String): List<Movie>
}