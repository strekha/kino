package kino.dao

import kino.entity.Person
import org.springframework.stereotype.Repository
import javax.persistence.EntityManager

interface PersonsDao : Dao<Person>

@Repository
open class PersonsDaoImpl(private val manager: EntityManager) : BaseDao<Person>(manager), PersonsDao {

    override fun getById(id: Int) = manager.find(Person::class.java, id)

    override fun getAll() = manager.createQuery("FROM Person", Person::class.java).resultList

}