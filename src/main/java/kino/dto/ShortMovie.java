package kino.dto;

import kino.entity.Movie;

public class ShortMovie {

    private int id;
    private String titleRus;
    private String titleEng;
    private int year;
    private double rating;

    public ShortMovie(int id, String titleRus, String titleEng, int year, double rating) {
        this.id = id;
        this.titleRus = titleRus;
        this.titleEng = titleEng;
        this.year = year;
        this.rating = rating;
    }

    public ShortMovie(Movie movie) {
        this.id = movie.getId();
        this.titleRus = movie.getTitleRus();
        this.titleEng = movie.getTitleEng();
        this.year = movie.getYear();
        this.rating = movie.getRating();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitleRus() {
        return titleRus;
    }

    public void setTitleRus(String titleRus) {
        this.titleRus = titleRus;
    }

    public String getTitleEng() {
        return titleEng;
    }

    public void setTitleEng(String titleEng) {
        this.titleEng = titleEng;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }
}
