package kino.dto;

import kino.entity.Movie;
import kino.entity.Person;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

public class DetailedPerson {

    private int id;
    private String firstName;
    private String lastName;
    private List<ShortMovie> directed;
    private List<ShortMovie> starred;

    public DetailedPerson(Person person) {
        id = person.getId();
        firstName = person.getFirstName();
        lastName = person.getLastName();
        directed = person.getDirected().stream().map(ShortMovie::new).collect(toList());
        starred = person.getStarred().stream().map(ShortMovie::new).collect(toList());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<ShortMovie> getDirected() {
        return directed;
    }

    public void setDirected(List<ShortMovie> directed) {
        this.directed = directed;
    }

    public List<ShortMovie> getStarred() {
        return starred;
    }

    public void setStarred(List<ShortMovie> starred) {
        this.starred = starred;
    }
}
