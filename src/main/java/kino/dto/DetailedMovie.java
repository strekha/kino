package kino.dto;

import kino.entity.Movie;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

public class DetailedMovie {

    private int id;
    private String titleRus;
    private String titleEng;
    private int year;
    private double rating;
    private List<ShortPerson> directors;
    private List<ShortPerson> actors;
    private List<ShortCountry> countries;
    private List<ShortGenre> genres;

    public DetailedMovie(Movie movie) {
        id = movie.getId();
        titleEng = movie.getTitleEng();
        titleRus = movie.getTitleRus();
        year = movie.getYear();
        rating = movie.getRating();
        directors = movie.getDirectors().stream().map(ShortPerson::new).collect(toList());
        actors = movie.getActors().stream().map(ShortPerson::new).collect(toList());
        countries = movie.getCountries().stream().map(ShortCountry::new).collect(toList());
        genres = movie.getGenres().stream().map(ShortGenre::new).collect(toList());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitleRus() {
        return titleRus;
    }

    public void setTitleRus(String titleRus) {
        this.titleRus = titleRus;
    }

    public String getTitleEng() {
        return titleEng;
    }

    public void setTitleEng(String titleEng) {
        this.titleEng = titleEng;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public List<ShortPerson> getDirectors() {
        return directors;
    }

    public void setDirectors(List<ShortPerson> directors) {
        this.directors = directors;
    }

    public List<ShortPerson> getActors() {
        return actors;
    }

    public void setActors(List<ShortPerson> actors) {
        this.actors = actors;
    }

    public List<ShortCountry> getCountries() {
        return countries;
    }

    public void setCountries(List<ShortCountry> countries) {
        this.countries = countries;
    }

    public List<ShortGenre> getGenres() {
        return genres;
    }

    public void setGenres(List<ShortGenre> genres) {
        this.genres = genres;
    }
}
