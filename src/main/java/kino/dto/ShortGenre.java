package kino.dto;

import kino.entity.Genre;

public class ShortGenre {

    private int id;
    private String name;

    public ShortGenre(Genre genre) {
        id = genre.getId();
        name = genre.getName();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
