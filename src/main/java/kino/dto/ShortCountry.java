package kino.dto;

import kino.entity.Country;

public class ShortCountry {

    private int id;
    private String name;

    public ShortCountry(Country country) {
        id = country.getId();
        name = country.getName();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
