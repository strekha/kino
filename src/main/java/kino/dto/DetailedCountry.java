package kino.dto;

import kino.entity.Country;

import java.util.List;
import java.util.stream.Collectors;

public class DetailedCountry {

    private int id;
    private String name;
    private List<ShortMovie> movies;

    public DetailedCountry(Country country) {
        id = country.getId();
        name = country.getName();
        movies = country.getMovies().stream().map(ShortMovie::new).collect(Collectors.toList());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ShortMovie> getMovies() {
        return movies;
    }

    public void setMovies(List<ShortMovie> movies) {
        this.movies = movies;
    }
}
