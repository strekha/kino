package kino.controllers

import kino.repository.CountriesRepository
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/country")
open class CountriesController(private val countriesRepository: CountriesRepository) {

    @GetMapping(value = "/getAll")
    fun getAllMovies() = countriesRepository.getAll()

    @GetMapping(value = "/filter")
    fun filter(@RequestParam(name = "id", required = true) id: Int) = countriesRepository.getById(id)
}