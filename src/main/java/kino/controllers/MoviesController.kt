package kino.controllers

import kino.repository.MoviesRepository
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/movies")
class MoviesController(private val moviesRepository: MoviesRepository) {

    @GetMapping(value = "/getAll")
    fun getAllMovies() = moviesRepository.getAllMovies()

    @GetMapping(value = "/filter")
    fun filter(@RequestParam(name = "id", required = true) id: Int) = moviesRepository.getById(id)
}
