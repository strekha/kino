package kino.controllers

import kino.repository.GenresRepository
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/genre")
open class GenresController(private val genresRepository: GenresRepository) {


    @GetMapping(value = "/getAll")
    fun getAllMovies() = genresRepository.getAll()

    @GetMapping(value = "/filter")
    fun filter(@RequestParam(name = "id", required = false) id: Int?,
               @RequestParam(name = "name", required = false) name: String?) = genresRepository.getById(id, name)
}