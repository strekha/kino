package kino.controllers

import kino.repository.PersonsRepository
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/persons")
class PersonsController(private val personsRepository: PersonsRepository) {

    @GetMapping(value = "/getAll")
    fun getAllPersons() = personsRepository.getAll()

    @GetMapping(value = "/filter")
    fun filter(@RequestParam(name = "id", required = true) id: Int) = personsRepository.getById(id)
}