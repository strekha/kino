package kino.exceptions

import java.lang.RuntimeException

class RequaredParamsNotPresentException : RuntimeException() {

    override val message: String?
        get() = "Request requires at least one argument!"
}