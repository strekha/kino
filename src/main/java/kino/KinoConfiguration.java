package kino;

import kino.dao.CountriesDao;
import kino.dao.GenresDao;
import kino.dao.MoviesDao;
import kino.dao.PersonsDao;
import kino.repository.CountriesRepository;
import kino.repository.GenresRepository;
import kino.repository.MoviesRepository;
import kino.repository.PersonsRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KinoConfiguration {

    @Bean
    public MoviesRepository movieRepository(MoviesDao moviesDao) {
        return new MoviesRepository(moviesDao);
    }

    @Bean
    public PersonsRepository personsRepository(PersonsDao personsDao) {
        return new PersonsRepository(personsDao);
    }

    @Bean
    public CountriesRepository countriesRepository(CountriesDao countriesDao) {
        return new CountriesRepository(countriesDao);
    }

    @Bean
    public GenresRepository genresRepository(GenresDao genresDao) {
        return new GenresRepository(genresDao);
    }
}
